from django.conf.urls.defaults import *

urlpatterns = patterns('maddenapp.views',
   url(r'^rate/(?P<userid>\d+)/$', 'rate_player', name='maddenapp_rate_player_now'),
   url(r'^rate/(?P<season>\d{4})/(?P<userid>\d+)/$', 'rate_player', name='maddenapp_rate_player_season'),
)