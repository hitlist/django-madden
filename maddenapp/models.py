from django.db import models
from django.contrib.auth.models import User
from hitmen.leaguemanager.models import Season
from hitmen.utils import ContentAwareModel
from django.conf import settings
from hashlib import sha1




class Rating(ContentAwareModel):

    # a user will only be able to manage a single instance on a given 
    # user, and will not be able to edit their own
    for_season           = models.ForeignKey(Season)
    to_user              = models.ForeignKey(User)
    from_user            = models.ForeignKey(User, related_name='maddenrater_set')
    # General Attributes
    general_speed        = models.PositiveIntegerField('Speed', default=0)
    general_agility      = models.PositiveIntegerField('Agility', default=0)
    general_acceleration = models.PositiveIntegerField('Acceleration', default=0)
    general_toughness    = models.PositiveIntegerField('Toughness', default=0)
    general_injury_prone = models.PositiveIntegerField('Injury Prone', default=0)
    general_strength     = models.PositiveIntegerField('Strength', default=0)
    general_catching     = models.PositiveIntegerField('Catching', default=0)
    general_intensity    = models.PositiveIntegerField('Intensity', default=0)
    general_stamina      = models.PositiveIntegerField('Stamina', default=0)
    general_awareness    = models.PositiveIntegerField('Awareness', default=0)
    general_aggresion    = models.PositiveIntegerField('Aggression', default=0)
    general_leadership   = models.PositiveIntegerField('Leadership', default=0)
    
    # Offensive Attributes
    offense_break_tackle = models.PositiveIntegerField('Break Tackle', default=0)
    offense_blocking     = models.PositiveIntegerField('Blocking', default=0)
    
    #QB
    qb_throw_acc         = models.PositiveIntegerField('Throwing Accuracy', default=0)
    qb_arm_str           = models.PositiveIntegerField('Arm Strength', default=0)
    qb_composure         = models.PositiveIntegerField('Composure', default=0)
    qb_scrambling        = models.PositiveIntegerField('Scramble', default=0)
    
    # TE / WR
    rec_runroute         = models.PositiveIntegerField('Route Running', default=0)
    rec_breakaway_spd    = models.PositiveIntegerField('Break-Away Speed', default=0)
    rec_run_after_catch  = models.PositiveIntegerField('Run After Catch', default=0)
    
    #rb/fb
    rb_balance           = models.PositiveIntegerField('Balance', default=0)
    rb_vision            = models.PositiveIntegerField('Vision', default=0)
    rb_power             = models.PositiveIntegerField('Power', default=0)
    rb_elusive           = models.PositiveIntegerField('Elusivness', default=0)
    
    # Oline
    oline_blitzpickup    = models.PositiveIntegerField('Blitz Pick-up', default=0)
    oline_pancake_block  = models.PositiveIntegerField('Pancake Block', default=0)
    oline_openfieldblock = models.PositiveIntegerField('Open Field Blocking', default=0)
    olline_composure     = models.PositiveIntegerField('Composure', default=0)
    
    # Defensive Attributes
    defense_tackle       = models.PositiveIntegerField('Tackling', default=0)
    defense_runstop      = models.PositiveIntegerField('Run Stopping', default=0)
    defense_beat_dblteam = models.PositiveIntegerField('Beat Double Team', default=0)
    defense_shedblock    = models.PositiveIntegerField('Shed Block', default=0)
    
    #defensive line
    dline_usehands       = models.PositiveIntegerField('Use Of Hands', default=0)
    dline_stunt          = models.PositiveIntegerField('Stunt Ability', default=0)
    dline_passrush       = models.PositiveIntegerField('Pass Rush', default=0)
    dline_playrecog      = models.PositiveIntegerField('Play Recognition', default=0)
    
    # secondary 
    secondary_blitzing   = models.PositiveIntegerField('Blitzing', default=0)
    secondary_coverage   = models.PositiveIntegerField('Pass Coverage', default=0)
    secondary_bumb_cover = models.PositiveIntegerField('Bump And Run', default=0)
    secondary_pursuit    = models.PositiveIntegerField('Pursuit', default=0)
    
    # Special Teams Attributes   
    kp_leg_str           = models.PositiveIntegerField('Leg Strength', default=0)
    kp_kick_accuracy     = models.PositiveIntegerField('Kicking Accuracy', default=0)
    kp_punt_accuracy     = models.PositiveIntegerField('Punting Accuracy', default=0)
    
    class Meta:
        unique_together = (('for_season', 'to_user', 'from_user'),)
        permissions = (
            ("rate_player","Can Rate Players"),
        )
     
    def __unicode__(self):
        return "%s's %s madden rating for %s" %(self.from_user.get_full_name(),self.for_season.seasons_year(), self.to_user.get_full_name())
    def get_rating_key(self):
        key = sha1('%s%s%d' % (self.to_user, self.from_user, self.pk) ).hexdigest()
        return key
    def get_rating(self, year=None):
        weightmap = getattr(settings, 'MADDENAPP_%s_RATING_WEIGHT' %self.to_user.get_profile().position )
        total_score = 0
        rating = 0
        _ratings = self.__dict__
        for k in weightmap.keys():
            total_score = total_score + _ratings[k] 
            #print "score: %s" %total_score
        rating = (total_score)
        if rating > 100:
            return 100
        else:
            return rating        
    def get_weighted_rating(self, year=None):
        key = self.get_rating_key()
        #import pdb
        weightmap = getattr(settings, 'MADDENAPP_%s_RATING_WEIGHT' %self.to_user.get_profile().position )
        total_score = 0
        rating = 0
        _ratings = self.__dict__
        for k in weightmap.keys():
            total_score = total_score + (_ratings[k] * ( float(weightmap[k])/100))
            print "score: %s" %total_score
        #pdb.set_trace()
        rating = int(total_score)
        if rating > 100:
            self.__REDIS__.set(key, 100)
            return 100
        else:
            self.__REDIS__.set(key, rating)
            return rating
    def get_absolute_url(self):
        return self.to_user.get_profile().get_absolute_url()
