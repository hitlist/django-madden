'''            MADDENAPP VIEWS.PY        '''
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.template import RequestContext
from django.shortcuts import get_object_or_404
from maddenapp.utils import compile_overall_rating, get_rating_form
from maddenapp.models import Rating
from hitmen.leaguemanager.models import Season
from hitmen.team.models import Member
from datetime import datetime
from django.conf import settings
@login_required 
@permission_required('maddenapp.rate_player')
def rate_player(request, userid, season=None):
    
    user = User.objects.get(pk=userid)
    member = get_object_or_404(Member, user = user)
    point_status = None
    if not member.is_player():
        return HttpResponseRedirect(request.META['HTTP_REFERER'] or '/')
		
    if season is not None:
        cur_season = Season.objects.get(season_year__year = season)
    else:
        cur_season = Season.objects.latest()

    form_class = get_rating_form(member.position)
	# try to find the Rating we want to edit
    try:
        rating = Rating.objects.get(from_user = request.user, to_user = user, for_season = cur_season)
    except:
        rating = None
    #if we got some data
    if request.POST:
        if rating is not None:
            form = form_class(request.POST, instance=rating)
        else:
            form = form_class(request.POST)
        if form.is_valid():
            new_item = form.save()
            return HttpResponseRedirect(member.get_absolute_url())
    # if we didn't get some data
    else:
        if rating is not None:
            form = form_class(instance = rating)
        else:
            form = form_class(initial={
                               'for_season':cur_season.pk,
                               'to_user':user.pk,
                               'from_user':request.user.pk
                              }
                       )
    max_points = getattr(settings, 'MADDENAPP_BASE_POINTS_ALLOWED', 800)
    if member.captain_status:
        max_points = max_points + getattr(settings, 'MADDENAPP_CAPTAIN_POINTS', 250) 
    if member.rank == 'allstar':
        max_points = max_points + getattr(settings, 'MADDENAPP_ALLSTAR_POINTS', 300)
    elif member.rank == 'veteran':
        max_points = max_points + getattr(settings, 'MADDENAPP_VETERAN_POINTS', 175)
    else:
        max_points = max_points + 50
    years_on_team = member.years_with_team()
    xfactor =  getattr(settings, 'MADDENAPP_XFACTOR', 0.1)
    exp_points = getattr(settings, 'MADDENAPP_YEARS_EXP_POINTS', 8)
    cofactor  = getattr(settings, 'MADDENAPP_XFACTOR_COFACTOR', 2)
    max_points = max_points + (getattr(settings, 'MADDENAPP_YEARS_ON_TEAM_POINTS', 25) * years_on_team)
    xfactor = xfactor * member.years_exp/cofactor
    max_points = max_points + member.years_exp * int( exp_points + ( (1 + xfactor) * exp_points ))

    return render_to_response('maddenapp/maddenapp_rate_player.html', {'form':form, 'max_points':max_points, 'MEMBER':member, 'OWNER':False}, context_instance=RequestContext(request))
