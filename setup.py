from setuptools import setup

setup(
    name="django-maddenapp",
    version="1.0.0",
    description="Dynamic Player ratings app for hitmen website",
    author="Eric Satterwhite",
    author_email="eric@codedependant.net",
    packages=[ "maddenapp" ],
    classifiers=[
        "Framework::Django",
        "Programming Language::Python2.7"
    ]
)
