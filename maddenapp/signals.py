from django.dispatch import Signal

player_was_rated = Signal(providing_args=['rating', 'rater', 'ratee','score'])
rating_was_created = Signal(providing_args=['rating','rater','ratee','score'])
