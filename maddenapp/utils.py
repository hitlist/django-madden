from django.conf import settings
from django import forms
from maddenapp.models import Rating
from maddenapp.signals import player_was_rated, rating_was_created
def compile_overall_rating(user, year=None): 
    ''' calculates the over all rating of the user using a weighted score'''
    if year is not None:
        ratings = user.rating_set.filter(for_season__season_year__year = year).values()
    else:
        ratings = user.rating_set.values()
    weightmap = getattr(settings, 'MADDENAPP_%s_RATING_WEIGHT' % user.get_profile().position, None )
    default = getattr(settings,'MADDENAPP_DEFAULT_WEIGHT', 2)

    total_score = 0
    ovr_rating = 0
    for rating in ratings:
        if weightmap:
            total_score = total_score + sum( [(rating[k] * (float(weightmap[k])/100) ) for k in weightmap.keys()] )
        else:
            total_score = total_score + sum( [(rating[k] * (float(default)/100) ) for k in rating.keys()] )   
    try:
        ovr_rating = total_score / ratings.count()
    except ZeroDivisionError:
        return None
        
    if ovr_rating > 100:
        return 100
    return ovr_rating


def get_rating_form(position):
    from hitmen.leaguemanager.models import Season
    from django.contrib.auth.models import User
    _positionmap = getattr(settings, 
                           'MADDENAPP_POSITION_MAP', 
                           {
                             'QB':('general','offense','qb'),
                             'RB':('general','offense','rb'),
                             'FB':('general','offense','rb'),
                             
                             'KR':('general','offense','rb'),
                             'PR':('general','offense','rb'),
                             
                             'WR':('general','offense','rec'),
                             'TE':('general', 'offense', 'rec'),
                             
                             'OG':('general', 'offense', 'ol'),
                             'OC':('general', 'offense', 'ol'),
                             'OT':('general', 'offense', 'ol',),
                             
                             'DT':('general', 'defense', 'dline'),
                             'DE':('general', 'defense', 'dline'),
                             'NT':('general', 'defense', 'dline'),
                             
                             'MLB':('general', 'defense', 'secondary'),
                             'OLB':('general', 'defense', 'secondary'),
                             'SS':('general', 'defense', 'secondary'),
                             'CB':('general', 'defense', 'secondary'),
                             'FS':('general', 'defense', 'secondary'),
                    	     'P':('general','kp'),
			     'K':('general','kp'),
			     'KP':('general', 'kp'),	 
                            }
                        )
    exclude_fields = []
    rating_fields = Rating._meta.fields
    categories = None
    for k in _positionmap.keys():
        if k == position.upper():
            categories = _positionmap[k]
    for field in rating_fields:
        if field.name.split('_')[0].lower() not in categories:
            if field.name in ['id', 'from_user', 'to_user', 'for_season']:
                pass
            else:
                exclude_fields.append(field)
    class _MaddenForm(forms.ModelForm):
        '''
            this is a dynamic for that will display 
            certain fields based on position
        '''
        users = User.objects.all()
        seasons = Season.objects.all()
        for_season = forms.ModelChoiceField(seasons, widget=forms.HiddenInput())
        to_user = forms.ModelChoiceField(users, widget=forms.HiddenInput())
        from_user = forms.ModelChoiceField(users, widget=forms.HiddenInput())
        class Meta:
            model = Rating
            exclude = tuple(["%s" % f.name for f in exclude_fields])
        def save(self):
            if self.instance.pk is None:
                new_rating = super(_MaddenForm, self).save()
#                from hitmen.team.models import MemberLogEntry, ADDITION
#                from django.contrib.contenttypes.models import ContentType
#                MemberLogEntry.objects.log_action(
#                    user_id         = new_rating.from_user.pk,
#                    content_type_id = ContentType.objects.get_for_model(new_rating).pk,
#                    object_id       = new_rating.pk,
#                    action_flag     = ADDITION,
#                    change_message  = 'has just rated'
#                )
                rating_was_created.send(sender=Rating, 
                                      rating=new_rating, 
                                      rater=new_rating.from_user, 
                                      ratee=new_rating.to_user,
                                      score=compile_overall_rating(new_rating.to_user))
            else:
                new_rating = super(_MaddenForm, self).save()
                player_was_rated.send(sender=Rating, 
                                      rating=new_rating, 
                                      rater=new_rating.from_user, 
                                      ratee=new_rating.to_user,
				      score=compile_overall_rating(new_rating.to_user))
            return new_rating
    return _MaddenForm
            
#            
#    for k, v in POSITION_MAP.iteritems():
#        if k == position.upper():
#            categories = POSITION_MAP[k]
#    for field in rating_fields:
#        if field.name.startswith(categories[0].lower()) or\
#                                 field.name.startswith(categories[1].lower()) or\
#                                 field.name.startswith(categories[2].lower()):
#            include_fields.append(field)
#        else:
#            exclude_fields.append(field)
#    fields = {}
#    for field in include_fields:
#        fields[field.name] = forms.CharField(max_length=100, 
#                                widget=forms.HiddenInput())
#    pdb.set_trace()
#    return type('MaddenForm', (ModelForm,), {'base_fields':fields})            
