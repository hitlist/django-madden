from django import forms
from django.forms import fields, BaseForm
from django.utils.datastructures import SortedDict
from django.conf import settings
from maddenapp.models import Rating
import pdb
class MaddenForm(forms.ModelForm):
    '''
        the madden_position_map is a maping 
        of the position of the player in 
        question to the prefeix of the field 
        categories we want to render in the form
    '''

    general_speed        = forms.CharField(widget=forms.HiddenInput())
    general_agility      = forms.CharField(widget=forms.HiddenInput())
    general_acceleration = forms.CharField(widget=forms.HiddenInput())
    general_toughness    = forms.CharField(widget=forms.HiddenInput())
    general_injury_prone = forms.CharField(widget=forms.HiddenInput())
    general_strength     = forms.CharField(widget=forms.HiddenInput())
    general_catching     = forms.CharField(widget=forms.HiddenInput())
    general_intensity    = forms.CharField(widget=forms.HiddenInput())
    general_stamina      = forms.CharField(widget=forms.HiddenInput())
    general_awareness    = forms.CharField(widget=forms.HiddenInput())
    general_aggresion    = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = Rating
